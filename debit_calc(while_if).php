<?php
/**
 * Рассчитывает сумму вклада
 * @param $rate
 * @param $term
 * @param $i
 * @param $sum
 * @return array
 */
function calc($rate, $term, $i, $sum)
{
    $arresult = array();
    while ($i <= $term) {
        $profit = $sum * (1 + $rate);
        $result = sprintf("%d месяц - сумма накоплений %0.2f \r\n", $i, $profit);
        $arresult [] = $result;
        $sum = $profit;
        $i++;
    }
    return $arresult;
}
//срок вклада в месяцах
$term = 24;
//сумма вклада
$sum = 10000;
//месячная процентная ставка (свыше 100 000)
$rate3 = 6.9 / 12 / 100;
//месячная процентная ставка (от 10 000 до 100 000)
$rate2 = 4.8 / 12 / 100;
//месячная процентная ставка (до 10 000)
$rate1 = 2.1 / 12 / 100;
//порядковый номер месяца
$i = 1;


if ($sum <= 10000) {
    $calc = calc($rate1, $term, $i, $sum);
} elseif ($sum > 10000 and $sum <= 10200) {
    $calc = calc($rate2, $term, $i, $sum);
} else {
    $calc = calc($rate3, $term, $i, $sum);
}
foreach ($calc as $item) {
    echo $item;
}
?>
